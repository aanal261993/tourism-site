﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for Adminclass
/// </summary>
public class Adminclass
{
    public SqlConnection cn = new SqlConnection();
    public SqlCommand cmd = new SqlCommand();

	public Adminclass()
	{
		//

		// TODO: Add constructor logic here
		//
         cn.ConnectionString = @"Data Source=.\SQLEXPRESS;AttachDbFilename=C:\Users\anie\Desktop\tourism site\App_Data\reg_database.mdf;Integrated Security=True;User Instance=True";
        cn.Open();

	}
     public void ins(string str)
    {
        cmd.CommandText = str;
        cmd.Connection = cn;
        cmd.ExecuteNonQuery();
        cn.Close();
    }
     public DataSet sql(string str)
     {
         SqlDataAdapter da = new SqlDataAdapter(str, cn);
         DataSet ds = new DataSet();
         da.Fill(ds);
         return ds;

     }
     public int check(string str)
     {
         cmd.CommandText = str;
         cmd.Connection = cn;

         if (cmd.ExecuteScalar() != null)
         {
             return 1;   
         }
         return 0;
     }
}

	

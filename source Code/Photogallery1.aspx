﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Photogallery1.aspx.cs" Inherits="Photogallery1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 452px;
        }
        .style2
        {
            width: 452px;
            height: 23px;
        }
        .style3
        {
            height: 23px;
        }
        .style4
        {
            width: 452px;
            height: 37px;
        }
        .style5
        {
            height: 37px;
        }
        .style6
        {
            height: 37px;
            width: 402px;
        }
        .style7
        {
            width: 402px;
        }
        .style8
        {
            height: 23px;
            width: 402px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <table style="width:100%;">
        <tr>
            <td class="style4">
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td class="style6">
                &nbsp;</td>
            <td class="style5">
            </td>
        </tr>
        <tr>
            <td class="style4">
                &nbsp;</td>
            <td class="style6">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Italic="True" 
                    Font-Size="Medium" Text="Places of kerla"></asp:Label>
            </td>
            <td class="style5">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style1">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/image/aihole kerla.JPG" />
                <asp:Panel ID="Panel1" runat="server">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Aihole</asp:Panel>
            </td>
            <td class="style7">
                <asp:Image ID="Image2" runat="server" 
                    ImageUrl="~/image/alappuzha-alleppey kerla.jpg" />
                <asp:Panel ID="Panel2" runat="server">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Alappuzha-alleppey
                </asp:Panel>
            </td>
            <td>
                <asp:Image ID="Image3" runat="server" 
                    ImageUrl="~/image/Andaman and Nicobar kerla.jpg" />
                <asp:Panel ID="Panel3" runat="server">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Andaman and Nicobar
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="style2">
                <asp:Image ID="Image4" runat="server" 
                    ImageUrl="~/image/Athirapally kerla.jpg" />
                <asp:Panel ID="Panel4" runat="server">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Athirapally</asp:Panel>
            </td>
            <td class="style8">
                <asp:Image ID="Image5" runat="server" ImageUrl="~/image/Badami.jpg" />
                <asp:Panel ID="Panel5" runat="server">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Badami</asp:Panel>
            </td>
            <td class="style3">
                <asp:Image ID="Image6" runat="server" ImageUrl="~/image/Bagalkot kerla.jpg" />
                <asp:Panel ID="Panel6" runat="server">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bagalkot
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="style2">
                <asp:Image ID="Image7" runat="server" ImageUrl="~/image/Bandipur kerla.jpg" />
                <asp:Panel ID="Panel7" runat="server" style="margin-bottom: 0px">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bandipur
                </asp:Panel>
            </td>
            <td class="style8">
                <asp:Image ID="Image8" runat="server" 
                    ImageUrl="~/image/Bangalore - Bengal. kerla.jpg" />
                <asp:Panel ID="Panel8" runat="server">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bangalore - Bengal</asp:Panel>
            </td>
            <td class="style3">
                <asp:Image ID="Image9" runat="server" ImageUrl="~/image/Bekal kerla.jpg" />
                <asp:Panel ID="Panel9" runat="server">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Belur</asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="style2">
                <asp:Image ID="Image10" runat="server" ImageUrl="~/image/Bhatkal k.jpg" />
                <asp:Panel ID="Panel10" runat="server">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bhatkal
                </asp:Panel>
            </td>
            <td class="style8">
                <asp:Image ID="Image19" runat="server" ImageUrl="~/image/Bidar k.jpg" />
                <asp:Panel ID="Panel11" runat="server">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bidar
                </asp:Panel>
            </td>
            <td class="style3">
                <asp:Image ID="Image28" runat="server" ImageUrl="~/image/Bijapur k.jpg" />
                <asp:Panel ID="Panel12" runat="server">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Chettinad
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="style2">
                <asp:Image ID="Image11" runat="server" ImageUrl="~/image/Chidambaram k.jpg" />
                <asp:Panel ID="Panel13" runat="server">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Chettinad
                </asp:Panel>
            </td>
            <td class="style8">
                <asp:Image ID="Image20" runat="server" ImageUrl="~/image/Chikamangalur k.jpg" />
                <asp:Panel ID="Panel14" runat="server">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Chikamangalur
                </asp:Panel>
            </td>
            <td class="style3">
                <asp:Image ID="Image29" runat="server" ImageUrl="~/image/Chitradurgak.jpg" />
                <asp:Panel ID="Panel15" runat="server">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Chitradurga</asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="style2">
                <asp:Image ID="Image12" runat="server" 
                    ImageUrl="~/image/Cochin - Kochi k.jpg" />
                <asp:Panel ID="Panel16" runat="server">
                    Cochin - Kochi
                </asp:Panel>
            </td>
            <td class="style8">
                <asp:Image ID="Image21" runat="server" ImageUrl="~/image/Coimbatore k.jpg" />
                <asp:Panel ID="Panel17" runat="server">
                    Coimbatore
                </asp:Panel>
            </td>
            <td class="style3">
                <asp:Image ID="Image30" runat="server" ImageUrl="~/image/Coonoor k.jpg" />
                <asp:Panel ID="Panel18" runat="server">
                    Coonoor
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="style2">
                <asp:Image ID="Image13" runat="server" ImageUrl="~/image/Gavi k.jpg" />
                <asp:Panel ID="Panel19" runat="server">
                    Gavi
                </asp:Panel>
            </td>
            <td class="style8">
                <asp:Image ID="Image22" runat="server" ImageUrl="~/image/Gokarna k.jpg" />
                <asp:Panel ID="Panel20" runat="server">
                    Gokarna
                </asp:Panel>
            </td>
            <td class="style3">
                <asp:Image ID="Image31" runat="server" ImageUrl="~/image/Hassan k.jpg" />
                <asp:Panel ID="Panel21" runat="server">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Hassan
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="style2">
                <asp:Button ID="Button1" runat="server" Text="Next" />
            </td>
            <td class="style8">
                &nbsp;</td>
            <td class="style3">
                &nbsp;</td>
        </tr>
    </table>
    </form>
</body>
</html>

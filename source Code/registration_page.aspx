﻿<%@ Page  Language="C#" MasterPageFile="~/Master1.master" AutoEventWireup="true"
    CodeFile="registration_page.aspx.cs" Inherits="registration_page" %>
    <%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>


<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>


<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">


    <form id="form1">
    <div>
    
        <table style="width:100%;" align="center">
            <tr>
                <td class="style4">
                    <asp:Label ID="Label1" runat="server" Text="First Name :-" ForeColor="#FF0066"></asp:Label>
                </td>
                <td class="style5">
                    <asp:TextBox ID="Text1" runat="server"></asp:TextBox>
                    <asp:FilteredTextBoxExtender ID="Text1_FilteredTextBoxExtender" runat="server" 
                        TargetControlID="Text1" ValidChars="abcdefghijklmnopqrstuvwxyz">
                    </asp:FilteredTextBoxExtender>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="Text1" 
                        ErrorMessage="Name should be filled"></asp:RequiredFieldValidator>
                </td>
                <td class="style5">
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    <asp:Label ID="Label2" runat="server" Text="Middle Name :-" ForeColor="#FF5050"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="Text2" runat="server"></asp:TextBox>
                    <asp:FilteredTextBoxExtender ID="Text2_FilteredTextBoxExtender" runat="server" 
                        Enabled="True" TargetControlID="Text2" ValidChars="abcdefghijklmnopqrstuvwxyz">
                    </asp:FilteredTextBoxExtender>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="Text2" ErrorMessage="Middle name should be filled"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style1">
                    <asp:Label ID="Label3" runat="server" Text="Last Name :-" ForeColor="#FF5050"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                    <asp:FilteredTextBoxExtender ID="TextBox3_FilteredTextBoxExtender" 
                        runat="server" Enabled="True" 
                        TargetControlID="TextBox3" ValidChars="abcdefghijklmnopqrstuvwxyz">
                    </asp:FilteredTextBoxExtender>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label10" runat="server" ForeColor="#FF5050" 
                        Text="Address :-"></asp:Label>
                </td>
                <td class="style3">
                    <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                </td>
                <td class="style3">
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    <asp:Label ID="Label4" runat="server" Text="Phone no :-" ForeColor="#FF5050"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox5" runat="server" MaxLength="10"></asp:TextBox>
                    <asp:FilteredTextBoxExtender ID="TextBox5_FilteredTextBoxExtender" 
                        runat="server" Enabled="True" TargetControlID="TextBox5" 
                        ValidChars="0123456789">
                    </asp:FilteredTextBoxExtender>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style4">
                    <asp:Label ID="Label5" runat="server" Text="Alternate phone no :- " 
                        ForeColor="#FF5050"></asp:Label>
                </td>
                <td class="style5">
                    <asp:TextBox ID="TextBox6" runat="server" MaxLength="10"></asp:TextBox>
                    <asp:FilteredTextBoxExtender ID="TextBox6_FilteredTextBoxExtender" 
                        runat="server" Enabled="True" InvalidChars="abcdefghijklmnopqrstuvwxyz" 
                        TargetControlID="TextBox6" ValidChars="0123456789">
                    </asp:FilteredTextBoxExtender>
                </td>
                <td class="style5">
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    <asp:Label ID="Label6" runat="server" Text="Date of Birth :-" 
                        ForeColor="#FF5050"></asp:Label>
                </td>
                <td>
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                    <asp:TextBox ID="TextBox11" runat="server"></asp:TextBox>
                    <asp:CalendarExtender ID="TextBox11_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="TextBox11">
                    </asp:CalendarExtender>
                    <br />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style1">
                    <asp:Label ID="Label7" runat="server" Text="State :-" ForeColor="#FF5050"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList1" runat="server">
                        <asp:ListItem>Gujarat</asp:ListItem>
                        <asp:ListItem>Rajasthan</asp:ListItem>
                        <asp:ListItem>Andhra Pradesh</asp:ListItem>
                    </asp:DropDownList>
                    </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style1">
                    <asp:Label ID="Label8" runat="server" Text="City :-" ForeColor="#FF5050"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList2" runat="server">
                        <asp:ListItem>Ahmedabad</asp:ListItem>
                        <asp:ListItem>jaipur</asp:ListItem>
                        <asp:ListItem>mumbai</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style1">
                    <asp:Label ID="Label9" runat="server" Text="Email :-" ForeColor="#FF5050"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                        ControlToValidate="TextBox8" ErrorMessage="email should be filled " 
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    <asp:Panel ID="Panel1" runat="server">
                        </asp:Panel>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style1">
                    <asp:Label ID="Label13" runat="server" ForeColor="#FF5050" Text="User Id:-"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox9" runat="server"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style1">
                    <asp:Label ID="Label12" runat="server" Text="Password :-" ForeColor="#FF5050"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox10" runat="server" TextMode="Password"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr><td>
&nbsp;
                <asp:Button ID="Button2" runat="server" ForeColor="#FF0066" 
                    onclick="Button2_Click" Text="Submit" />
                &nbsp;<asp:Button ID="Button3" runat="server" Text="Reset" />
              </td> </td> 
            
        </table>
     </div>
    </form>
    <div style="background-color: #6600FF; height: 95px; margin-top: 414px;">
    </div>


</asp:Content>
﻿
<%@ Page  Language="C#" MasterPageFile="~/Master1.master" AutoEventWireup="true"
    CodeFile="Admintour_Detail.aspx.cs" Inherits="Admintour_Detail" %>



    <asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">

  
    <form id="form1">
    <div>
    
        <table style="width:100%;">
            <tr>
                <td>
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Travel id:" ForeColor="Red"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="Travel_id" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="To place:" ForeColor="#FF3300"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="To_place" runat="server" ontextchanged="TextBox2_TextChanged"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="From place:" ForeColor="#FF3300"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="From_place" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    <asp:Label ID="Label4" runat="server" Text="No of days:" ForeColor="#FF3300"></asp:Label>
                </td>
                <td class="style1">
                    <asp:TextBox ID="No_of_days" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label5" runat="server" Text="No of person:" ForeColor="#FF3300"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="No_of_person" runat="server" 
                        ontextchanged="TextBox5_TextChanged"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label6" runat="server" Text="Price:" ForeColor="#FF3300"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="Price" runat="server" ontextchanged="TextBox6_TextChanged"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label7" runat="server" Text="Place cover:" ForeColor="#FF3300"></asp:Label>
                </td>
                <td class="style2">
                    <asp:TextBox ID="Place_cover" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label8" runat="server" Text="Type of vehical:" 
                        ForeColor="#FF3300"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="Type_of_vehicle" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label9" runat="server" Text="Package type:" ForeColor="#FF3300"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="Package_type" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Done" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                        DataKeyNames="Travel_id" DataSourceID="SqlDataSource1" 
                        onselectedindexchanged="GridView1_SelectedIndexChanged">
                        <Columns>
                            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" 
                                ShowSelectButton="True" />
                            <asp:BoundField DataField="Travel_id" HeaderText="Travel_id" ReadOnly="True" 
                                SortExpression="Travel_id" />
                            <asp:BoundField DataField="To_place" HeaderText="To_place" 
                                SortExpression="To_place" />
                            <asp:BoundField DataField="From_place" HeaderText="From_place" 
                                SortExpression="From_place" />
                            <asp:BoundField DataField="No_of_days" HeaderText="No_of_days" 
                                SortExpression="No_of_days" />
                            <asp:BoundField DataField="No_of_person" HeaderText="No_of_person" 
                                SortExpression="No_of_person" />
                            <asp:BoundField DataField="Price" HeaderText="Price" SortExpression="Price" />
                            <asp:BoundField DataField="Place_cover" HeaderText="Place_cover" 
                                SortExpression="Place_cover" />
                            <asp:BoundField DataField="Type_of_vehical" HeaderText="Type_of_vehical" 
                                SortExpression="Type_of_vehical" />
                            <asp:BoundField DataField="Package_type" HeaderText="Package_type" 
                                SortExpression="Package_type" />
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                        DeleteCommand="DELETE FROM [Tourdetail] WHERE [Travel_id] = @Travel_id" 
                        InsertCommand="INSERT INTO [Tourdetail] ([Travel_id], [To_place], [From_place], [No_of_days], [No_of_person], [Price], [Place_cover], [Type_of_vehical], [Package_type]) VALUES (@Travel_id, @To_place, @From_place, @No_of_days, @No_of_person, @Price, @Place_cover, @Type_of_vehical, @Package_type)" 
                        SelectCommand="SELECT * FROM [Tourdetail]" 
                        UpdateCommand="UPDATE [Tourdetail] SET [To_place] = @To_place, [From_place] = @From_place, [No_of_days] = @No_of_days, [No_of_person] = @No_of_person, [Price] = @Price, [Place_cover] = @Place_cover, [Type_of_vehical] = @Type_of_vehical, [Package_type] = @Package_type WHERE [Travel_id] = @Travel_id">
                        <DeleteParameters>
                            <asp:Parameter Name="Travel_id" Type="String" />
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="Travel_id" Type="String" />
                            <asp:Parameter Name="To_place" Type="String" />
                            <asp:Parameter Name="From_place" Type="String" />
                            <asp:Parameter Name="No_of_days" Type="Decimal" />
                            <asp:Parameter Name="No_of_person" Type="Decimal" />
                            <asp:Parameter Name="Price" Type="Decimal" />
                            <asp:Parameter Name="Place_cover" Type="String" />
                            <asp:Parameter Name="Type_of_vehical" Type="String" />
                            <asp:Parameter Name="Package_type" Type="String" />
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="To_place" Type="String" />
                            <asp:Parameter Name="From_place" Type="String" />
                            <asp:Parameter Name="No_of_days" Type="Decimal" />
                            <asp:Parameter Name="No_of_person" Type="Decimal" />
                            <asp:Parameter Name="Price" Type="Decimal" />
                            <asp:Parameter Name="Place_cover" Type="String" />
                            <asp:Parameter Name="Type_of_vehical" Type="String" />
                            <asp:Parameter Name="Package_type" Type="String" />
                            <asp:Parameter Name="Travel_id" Type="String" />
                        </UpdateParameters>
                    </asp:SqlDataSource>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    
    </div>
    </form>
    <div style="background-color: #6600FF; height: 95px; margin-top: 414px;">
    </div>


</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Photo gallery.aspx.cs" Inherits="Photo_gallery" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style3
        {
            width: 274px;
        }
        .style4
        {
            width: 270px;
        }
        .style5
        {
            width: 274px;
            height: 198px;
        }
        .style6
        {
            width: 270px;
            height: 198px;
        }
        .style7
        {
            height: 198px;
        }
    </style>
</head>
<body >

    <form id="form1" runat="server">
    <div>
    
        <table style="width:100%";">
            <tr>
                <td class="style3">
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                </td>
                <td class="style4">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style3">
                    &nbsp;</td>
                <td class="style4">
                    <asp:Label ID="Label1" runat="server" BackColor="#9933FF" BorderColor="#6699FF" 
                        BorderStyle="Double" BorderWidth="10px" Font-Bold="True" Font-Italic="True" 
                        Font-Size="20pt" Font-Strikeout="False" Text="PHOTO GALLERY" Width="234px"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr></center>
                <td class="style3">
                    &nbsp;</td>
                <td class="style4">
                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Italic="True" 
                        Font-Size="Medium" Text="PLACES OF RAJASTHAN"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style3">
                    <asp:Image ID="Image1" runat="server" Height="166px" 
                        ImageUrl="~/image/alwar-fort.jpg" Width="229px" />
                    <asp:Panel ID="Panel1" runat="server">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Alwar-Fort</asp:Panel>
                </td>
                <td class="style4">
                    <asp:Image ID="Image2" runat="server" Height="168px" 
                        ImageUrl="~/image/bharatpur-bidr-sanctuary.jpg" Width="234px" />
                    <asp:Panel ID="Panel2" runat="server">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bharatpur-bidr-sanctuary</asp:Panel>
                </td>
                <td>
                    <asp:Image ID="Image3" runat="server" Height="160px" 
                        ImageUrl="~/image/desert-of-rajasthan.jpg" Width="241px" />
                    <asp:Panel ID="Panel3" runat="server">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Desert-of-rajasthan</asp:Panel>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    <asp:Image ID="Image4" runat="server" Height="168px" 
                        ImageUrl="~/image/dilwara-jain-temples.jpg" Width="239px" />
                    <asp:Panel ID="Panel4" runat="server">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Dilwara-jain-temples</asp:Panel>
                </td>
                <td class="style4">
                    <asp:Image ID="Image5" runat="server" Height="172px" 
                        ImageUrl="~/image/fort-of-mehrangarh.jpg" Width="230px" />
                    <asp:Panel ID="Panel5" runat="server">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fort-of-mehrangarh</asp:Panel>
                </td>
                <td>
                    <asp:Image ID="Image6" runat="server" Height="167px" 
                        ImageUrl="~/image/hawa-mahal-of-rajasthan.jpg" Width="235px" />
                    <asp:Panel ID="Panel6" runat="server">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Hawa-Mahal</asp:Panel>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    <asp:Image ID="Image7" runat="server" 
                        ImageUrl="~/image/jaisalmer-fort-rajasthan.jpg" />
                    <asp:Panel ID="Panel10" runat="server">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; jaisalmer-fort-rajasthan</asp:Panel>
                </td>
                <td class="style4">
                    <asp:Image ID="Image8" runat="server" ImageUrl="~/image/jal-mahal-jaipur.jpg" />
                    <asp:Panel ID="Panel11" runat="server">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; jal-mahal-jaipur</asp:Panel>
                </td>
                <td>
                    <asp:Image ID="Image9" runat="server" 
                        ImageUrl="~/image/junagarh-fort-bikaner.jpg" />
                    <asp:Panel ID="Panel7" runat="server">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; junagarh-fort-bikaner</asp:Panel>
                </td>
            </tr>
            <tr>
                <td class="style5">
                    <asp:ImageMap ID="ImageMap1" runat="server" ImageUrl="~/image/lake-palace.jpg">
                    </asp:ImageMap>
                    <asp:Panel ID="Panel9" runat="server">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; lake-palace</asp:Panel>
                </td>
                <td class="style6">
                    <asp:Image ID="Image10" runat="server" ImageUrl="~/image/mandawa-castle.jpg" />
                    <asp:Panel ID="Panel12" runat="server">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; mandawa-castle</asp:Panel>
                </td>
                <td class="style7">
                    <asp:Image ID="Image11" runat="server" 
                        ImageUrl="~/image/pushkar-camel-fair.jpg" />
                    <asp:Panel ID="Panel8" runat="server">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; pushkar-camel-fair</asp:Panel>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    <asp:Image ID="Image12" runat="server" 
                        ImageUrl="~/image/rajasthan-camel-festival.jpg" />
                    <asp:Panel ID="Panel13" runat="server">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; rajasthan-camel-festival</asp:Panel>
                </td>
                <td class="style4">
                    <asp:Image ID="Image13" runat="server" 
                        ImageUrl="~/image/ranthambhore-national-park.jpg" />
                    <asp:Panel ID="Panel14" runat="server">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ranthambhore-national-park</asp:Panel>
                </td>
                <td>
                    <asp:Image ID="Image14" runat="server" 
                        ImageUrl="~/image/rajasthan-Karni mata temple.jpg" />
                    <asp:Panel ID="Panel15" runat="server">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; rajasthan-Karni mata temple</asp:Panel>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    <asp:Image ID="Image15" runat="server" 
                        ImageUrl="~/image/sariska-national-park.jpg" />
                    <asp:Panel ID="Panel16" runat="server">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; sariska-national-park</asp:Panel>
                </td>
                <td class="style4">
                    <asp:Image ID="Image16" runat="server" 
                        ImageUrl="~/image/udaipur-lake-palace.jpg" />
                    <asp:Panel ID="Panel17" runat="server">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; udaipur-lake-palace</asp:Panel>
                </td>
                <td>
                    <asp:Image ID="Image17" runat="server" 
                        ImageUrl="~/image/umaid-bhawan-palace-jodhpur.jpg" />
                    <asp:Panel ID="Panel18" runat="server">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; umaid-bhawan-palace-jodhpur</asp:Panel>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    <asp:Button ID="Button1" runat="server" onclick="Button1_Click1" Text="Next" />
                </td>
                <td class="style4">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>

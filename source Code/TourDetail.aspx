﻿ 
<%@ Page  Language="C#" MasterPageFile="~/Master1.master" AutoEventWireup="true"
    CodeFile="Admintour_Detail.aspx.cs" Inherits="Admintour_Detail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">

    <form id="form1" >
    <div>
        <table style="width: 100%;">
            <tr>
                <td>
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="To place:" ForeColor="#FF3300"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList1" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="From place:" ForeColor="#FF3300"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList2" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    <asp:Label ID="Label4" runat="server" Text="No of days:" ForeColor="#FF3300"></asp:Label>
                </td>
                <td class="style1">
                    <asp:DropDownList ID="dlNoOfDays" runat="server">
                    <asp:ListItem>Select No.of Days</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label5" runat="server" Text="No of person:" ForeColor="#FF3300"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                    <asp:NumericUpDownExtender ID="TextBox2_NumericUpDownExtender" runat="server" Enabled="True"
                        Maximum="1.7976931348623157E+308" Minimum="-1.7976931348623157E+308" RefValues=""
                        ServiceDownMethod="" ServiceDownPath="" ServiceUpMethod="" Tag="" TargetButtonDownID=""
                        TargetButtonUpID="" TargetControlID="TextBox2" Width="0">
                    </asp:NumericUpDownExtender>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    <asp:Label ID="Label6" runat="server" Text="Price:" ForeColor="#FF3300"></asp:Label>
                </td>
                <td class="style1">
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label7" runat="server" Text="Place cover:" ForeColor="#FF3300"></asp:Label>
                </td>
                <td class="style2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label8" runat="server" Text="Type of vehical:" 
                        ForeColor="#FF3300"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList4" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label9" runat="server" Text="Package type:" ForeColor="#FF3300"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList3" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Done" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
     </div>
    </form>
    <div style="background-color: #6600FF; height: 95px; margin-top: 414px;">
    </div>


</asp:Content>
